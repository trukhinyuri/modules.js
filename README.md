#modules.js framework
###The framework for creating modular web clients

Copyright 2012-2015 Yuri Trukhin

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Official website (Coming soon http://webstore.pw/modulesjs)](http://webstore.pw/modulesjs)

[Main GIT Repository](http://alm.geo4geo.com/gitblit/summary/modules.js.git) (Login: guest , Password: guest)

[Main Bug Tracker](http://alm.geo4geo.com/youtrack) -> Login as guest -> Issues Page

[Main Test Server](http://alm.geo4geo.com/teamcity) -> Login as guest

[Repository @GitHub](https://github.com/trukhinyuri/modules.js)

[Bugtracker @GitHub](https://github.com/trukhinyuri/modules.js/issues?state=open")

[Repository @BitBucket](https://bitbucket.org/trukhinyuri/modules.js)

[Bugtracker @BitBucket](https://bitbucket.org/trukhinyuri/modules.js/issues?status=new&status=open)

See modules.js api documentation and examples in **modules.js/jsDoc** folder.

[Please, DONATE to the modules.js project!](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CFZMK8KLFSJC4)

###Current status
Preparing to 1.0 beta (updated)
